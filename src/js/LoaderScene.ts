export default class LoaderScene extends Phaser.Scene {
  public preload() {
    this.load.spritesheet("blocks", "./assets/images/tiles.png",{ frameWidth: 32, frameHeight: 32 });
    this.load.image("bg", "./assets/images/bg.png");
  }

  public create() {
    this.scene.start("game");
  }
}
