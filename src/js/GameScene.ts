import Block from "./Block";

export default class GameScene extends Phaser.Scene {
  public pills;
  public blocks;
  public background;
  public container: Phaser.GameObjects.Container;
  public selected: Array<Block>;
  public selecting: Boolean;
  public busy:Boolean;

  constructor() {
    super({
      key: "game",
      active: false,
      visible: false,
    });
  }

  public create() {

    this.background = this.add.image(160, 255, 'bg');

    this.blocks = [];
    this.pills = [];
    this.selected = [];
    this.container = this.add.container(40,40);
    this.selecting = false;
    this.busy = false;

    this.pills = Array(6);
    for (let i = 0; i < 6; i++) {
      this.pills[i] = Array(10).fill(null);
    }

    for (let x=0; x<6; x++){
      for (let y=0; y<10; y++){
      
        let color = Math.floor(Math.random() * 5);

        let block = new Block(this, (x*40)+20, (y*40)+20, 32, 32, color);
        block.p_x = x;
        block.p_y = y;

        this.pills[x][y] = block;
        this.blocks.push(block);
        
      } 
      
    }

    this.container.add(this.blocks);

    
    
    this.input.on('pointerup',function(){

      this.busy = true;

      this.selecting = false;

      let cols = [0,0,0,0,0,0];

      if(this.selected.length>2){

        for(let x=0;x<6;x++){
          for(let y=0;y<10;y++){
            if(this.pills[x][y].selected==true){
              cols[x]++;

              this.pills[x][y].new_y = cols[x];
              this.pills[x][y].tween_removed.play();
              
              let temp = this.pills[x][y];
              this.pills[x].splice(y,1);
              this.pills[x].unshift(temp);
              
            }
          }
        }

      }else{
        for(let i=0;i<this.selected.length;i++){
          this.selected[i].selected = false;
          this.selected[i].removed = false;
        }
        for(let i=0;i<this.selected.length;i++){
          this.selected[i].tween_restore.play();
        }

        this.busy = false;
      }

      this.selected = [];

    }.bind(this));
  }

  public update() {}

  moveBlocks(){
    for(let x=0;x<6;x++){
      for(let y=0;y<10;y++){
        this.pills[x][y].p_y = y;
        let new_y = ((y*40)+20);
        this.fallAnimation(this.pills[x][y],new_y);
      }
    }

    this.selected = [];
    this.busy = false;
  }

  fallAnimation(block,y){
    this.tweens.add({
      targets: block,
      y: y,
      ease: 'Bounce',
      duration: 700,
      paused:false,
      repeat: 0,
    });
  }
}
