import "phaser";
import LoaderScene from "./LoaderScene";
import GameScene from "./GameScene";

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'puzzle-slide-game',
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: 320,
    height: 520
  },
  render: {
    pixelArt: true,
    antialias: false,
    antialiasGL: false,
  },
  scene: [LoaderScene, GameScene],
};

window.addEventListener("load", () => {
  new Phaser.Game(config);
});
