import { Tweens } from "phaser";
import GameScene from "./GameScene";

export default class Block extends Phaser.GameObjects.TileSprite {
  public scene: GameScene;
  public p_x: number;
  public p_y: number;
  public new_y:number;
  public selected: Boolean;
  public frame_key:number;
  public tween_removed:Phaser.Tweens.Tween;
  public tween_selection:Phaser.Tweens.Tween;
  public tween_restore:Phaser.Tweens.Tween;
  public removed:Boolean;

  constructor(scene: GameScene, x: number, y: number, width:number, height:number, key: number) {
    const texture = "blocks";
    
    super(scene, x, y, width, height, texture, key);

    this.setInteractive();

    this.scene.add.existing(this);

    this.selected = false;
    this.removed = false;
    this.frame_key = key;

    this.tween_removed = this.scene.tweens.add({
      targets: this,
      scale: 0,
      ease: 'Linear',
      duration: 300,
      paused:true,
      repeat: 0,
      onComplete: function () {

        this.y = (-(this.new_y*40)+20);
        this.p_y = -this.new_y;
        
        this.selected = false;
        this.removed = true;
        this.scale = 1;
        this.changeColor();

        let test = this.scene.selected.filter(row => row.removed==true);
        if(test.length==this.scene.selected.length){
          this.scene.moveBlocks();
        }
      }.bind(this)
    });

    this.tween_selection = this.scene.tweens.add({
      targets: this,
      scale: 1.1,
      ease: 'Linear',
      duration: 100,
      paused:true,
      repeat: 0,
    });

    this.tween_restore = this.scene.tweens.add({
      targets: this,
      scale: 1,
      ease: 'Linear',
      duration: 100,
      paused:true,
      repeat: 0,
    });

    this.addListener("pointerdown",function(){
      if(this.scene.busy==false){
        this.selected = true;
        this.scene.selecting = true;
        this.scene.selected.push(this);
        this.tween_selection.play();
      }
    });
      
    this.addListener("pointerover",function(){
      if(this.scene.selecting==true && this.scene.busy==false){
        let latest = this.scene.selected[this.scene.selected.length-1];

        let dist_x = Math.abs(this.p_x-latest.p_x);
        let dist_y = Math.abs(this.p_y-latest.p_y);

        if(latest.frame_key==this.frame_key && dist_x<2 && dist_y<2 && (latest.p_x!=this.p_x || latest.p_y!=this.p_y)){
          if(this.selected==true){
            //come back to the previous block, disable latest selcted block
            let index = this.scene.selected.findIndex(row => row.p_x==latest.p_x && row.p_y==latest.p_y);
            this.scene.selected.splice(index,1);
            latest.selected = false;
            latest.tween_restore.play();
          }else{
            //select new block
            this.tween_selection.play();
            this.selected = true;
            this.scene.selected.push(this);
          }
        }
      }
    }.bind(this));

  }

  changeColor(){
    let key = Math.floor(Math.random() * 5);
    this.frame_key = key;
    this.setFrame(key);
  }
}
